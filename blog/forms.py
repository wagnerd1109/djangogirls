#!/usr/bin/env python

__author__ = "Dennis Wagner"
__email__ = "dennis.wagner@th-brandenburg.de"

from django import forms

from .models import Post
from .models import Comment


class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'text',)


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('author', 'text',)